import manageConnectDatabase from 'utils/db'
import { registerStation, getSystemConfigs } from './lib/registerOrganizations'
import { createConnection } from 'shared/mongo-utils/connect'
import createStationAutoModel from 'models/StationAuto'
import stationAutoDao from 'dao/stationAutoDao'

const organization = {
  _id: '5b766a0bc0387b0010b50772',
  databaseInfo: {
    onDemand: true,
    port: '27018',
    address: '27.74.251.0',
    pwd: 'happy2code',
    user: 'dev',
    name: 'ilotusland_cem'
  },
  ftpPath: '/envisoft_cem'
}

async function getStations () {
  try {
    const mongoDbConnect = await createConnection(organization.databaseInfo)
    const stationAutoModel = createStationAutoModel(mongoDbConnect)
    const stations = await stationAutoDao.getConfiguredLogger(
      stationAutoModel,
      {
        $or: [{ key: 'TanHanh_NUO' }]
      }
    )
    return { success: true, stations: stations }
  } catch (e) {
    return { error: true, message: e.message }
  }
}

async function main () {
  await manageConnectDatabase()
  try {
    const warningConfigs = await getSystemConfigs()
    const { stations } = await getStations()
    console.log(stations)
    // console.log(warningConfigs)
    registerStation({ organization, station: stations[0], warningConfigs })
    // registerStation({ organization, station: stations[1], warningConfigs })
  } catch (e) {
    console.log(e)
  }
  // registerOrganizations()
}

main()
