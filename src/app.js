import manageConnectDatabase from 'utils/db'
import registerOrganizations from './lib/registerOrganizations'

async function main () {
  await manageConnectDatabase()
  try {
    await registerOrganizations()
  } catch (e) {
    console.log(e)
  }
}

main()
