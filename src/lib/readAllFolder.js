import { registerOrganizations } from './registerOrganizations'
import path from 'path'
import config from 'config'
import logger from 'logger'
import fs from 'fs'
import Queue from 'better-queue'
import glob from 'glob'
import { Promise } from 'core-js'
import createQueue from './queueAction'
import { sv4ReadAndWriteToServer } from './sv24Readfile'

const q = createQueue()

export const qSearchFolder = new Queue(
  async function (input, cb) {
    if (!input.length) {
      const { func, params } = input
      await func(params)
      cb(null)
    } else {
      for (let i = 0; i < input.length; i++) {
        const { func, params } = input[i]
        await func(params)
      }
      cb(null)
    }
  },
  { batchSize: 1, batchDelayTimeout: 1000, afterProcessDelay: 300 }
)

export function searchFile ({
  pathStation,
  organization,
  station,
  warningConfigs
}) {
  return new Promise((resolve, reject) => {
    let pathWithSearchFile = path.join(
      pathStation,
      `**/${station.fileName}*.txt`
    )
    glob(pathWithSearchFile, {}, function (er, files) {
      logger.info(`${station.path}: ${files.length}`)
      files.forEach(filename => {
        q.push({
          params: {
            filename,
            organization,
            station,
            warningConfigs,
            q
          },
          func: sv4ReadAndWriteToServer
        })
      })
      resolve()
    })
  })
}

async function processData ({ station, organization, warningConfigs }) {
  try {
    let pathStation = path.join(config.folder.ftpPath, station.path)
    const isExits = await fs.existsSync(pathStation)
    if (!isExits || !station.fileName) {
      logger.error(`not exists ${pathStation}`)
      return
    }
    await searchFile({
      pathStation,
      organization,
      station,
      warningConfigs
    })
  } catch (e) {
    console.log(e)
    logger.error(`process: ${e.message}`)
  }
}

export default function register () {
  registerOrganizations(params => {
    qSearchFolder.push({
      params,
      func: processData
    })
  })
}
