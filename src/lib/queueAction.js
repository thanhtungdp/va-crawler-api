import Queue from 'better-queue'
import config from 'config'

export default function createQueue () {
  const qChild = new Queue(
    async function (inputChild, cbChild) {
      const { func, params } = inputChild
      await func(params)
      cbChild(null)
    },
    {
      afterProcessDelay: config.QUEUE_TOTAL_READ_CHILD,
      maxRetries: 50,
      retryDelay: 300
    }
  )

  const queue = new Queue(
    async function (input, cb) {
      for (let i = 0; i < input.length; i++) {
        qChild.push(input[i])
        // const { func, params } = input[i]
        // func(params)
      }
      cb(null)
    },
    {
      batchSize: config.QUEUE_TOTAL_READ,
      batchDelayTimeout: 10000,
      afterProcessDelay: 60
    }
  )
  return queue
}
