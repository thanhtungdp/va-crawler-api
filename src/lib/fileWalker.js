import sv24Readfile, { sv24ReadfileStream } from './sv24Readfile'
import { q } from './queueAction'
import config from 'config'
import path from 'path'
const filewalker = require('filewalker')

export function fileWalkerWatch() {
  var totalFile = 0
  let fi
  filewalker(config.folder.rootWatch)
    .on('dir', function(p) {
      // console.log('dir:  %s', p)
    })
    .on('file', async function(p, s) {
      // const filename = path.join(config.folder.rootWatch, p)
      // q.push({ func: sv24Readfile, params: { filename } })
      // console.log('file: %s, %d bytes', p, s.size)
    })
    .on('error', function(err) {
      console.error(err)
    })
    .on('done', function() {
      console.log(
        '%d dirs, %d files, %d bytes, %d totalFile',
        this.dirs,
        this.files,
        this.bytes,
        totalFile
      )
    })
    .walk()
}
