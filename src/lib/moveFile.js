import path from 'path'
import fs from 'fs'
import config from 'config'

export default async function moveFile ({ filename, type }) {
  function move (dirPath) {
    // move file
    let pathFile = filename.replace(config.folder.ftpPath, '')
    let newImportPath = `${dirPath}${pathFile}`
    if (!fs.existsSync(newImportPath.replace(path.basename(filename), ''))) {
      let paths = pathFile.split('/')
      let fullPath = dirPath
      paths.forEach(path => {
        if (path.indexOf('.txt') === -1) {
          fullPath = fullPath + '/' + path
          if (!fs.existsSync(fullPath)) {
            fs.mkdirSync(fullPath)
          }
        }
      })
    }
    fs.renameSync(filename, newImportPath)
  }
  switch (type) {
    case 'error':
      move(config.folder.ftpErrorPath)
      break
    case 'imported':
      move(config.folder.ftpImportedPath)
      break
  }
}
