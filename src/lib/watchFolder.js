import chokidar from 'chokidar'
import fs from 'fs'
import logger from 'logger'
import createQueue from './queueAction'
import sv24Readfile, { sv4ReadAndWriteToServer } from './sv24Readfile'
import { Promise } from 'mongoose'

const q = createQueue()

/**
 * Setup automatic watch folder
 * @param folder:String
 * @param callback: Func - with filename callback
 */
export async function watchFolderBase({ folder, callback, callbackDone }) {
  const start = new Date()
  const isExists = await fs.existsSync(folder)
  if (!isExists) {
    logger.error(`${folder} not exists`)
    callbackDone({ isNotExists: true })
    return
  }
  logger.info(`${folder} start watch`)
  let totalFile = 0
  chokidar
    .watch(folder, {
      ignored: /((IMPORTED_.*)|(^|[\/\\]))\../,
      ignorePermissionErrors: true
    })
    .on('add', filename => {
      if (filename) {
        totalFile++
        callback(filename)
      }
    })
    .on('ready', (e, path) => {
      const end = new Date()
      logger.info(`${folder} completed in ${end - start} ms`)
      if (callbackDone) {
        callbackDone({ totalFile })
      }
    })
    .on('error', e => {
      // logger.info(`watch error ${folder}`)
    })
}

/**
 * Watch folder For Test with Read file
 */
export function watchFolderForTest({ folder }) {
  return new Promise((resolve, reject) => {
    const q = createQueue()
    watchFolderBase({
      folder,
      callback: filename => {
        totalFileTmp++
        q.push({
          func: sv24Readfile,
          params: { filename, organization, station, q }
        })
      },
      callbackDone: ({ isNotExists, totalFile }) => {
        resolve()
      }
    })
  })
}

/**
 * Watch with folder and write to server
 */
export default async function watchFolder({
  organization,
  station,
  folder,
  warningConfigs
}) {
  return new Promise((resolve, reject) => {
    watchFolderBase({
      folder,
      callback: filename => {
        q.push({
          func: sv4ReadAndWriteToServer,
          params: { filename, organization, station, warningConfigs, q }
        })
      },
      callbackDone: ({ isNotExists, totalFile }) => {
        resolve()
        // if (isNotExists || !totalFile) {
        //   resolve()
        // } else {
        //   q.on('empty', () => {
        //     if (totalFileTmp === totalFile) {
        //       logger.info(`total ${folder} ${totalFile} - ${totalFileTmp}`)
        //       resolve()
        //     }
        //   })
        // }
      }
    })
  })
}

process.on('exit', () => {
  q.destroy(() => {
    console.log('destroy real')
  })
  console.log('detroy app')
})

process.on('SIGINT', () => {
  process.exit(2)
})
