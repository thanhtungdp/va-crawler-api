import dataStationAutoDao from 'dao/dataStationAutoDao'
import stationAutoDao from 'dao/stationAutoDao'
import createDataStationModel from 'models/DataStationAuto'
import createStationAutoModel from 'models/StationAuto'
import { createConnection } from 'shared/mongo-utils/connect'

export default async function importData ({
  organization,
  stationKey,
  data: { receivedAt, measuringLogs }
}) {
  try {
    const mongoDbConnect = await createConnection(organization.databaseInfo)
    const dataStationModel = createDataStationModel(stationKey, mongoDbConnect)
    const stationAutoModel = createStationAutoModel(mongoDbConnect)
    await dataStationAutoDao.createOrUpdate(
      {
        receivedAt,
        measuringLogs
      },
      dataStationModel
    )
    const resStationAuto = await stationAutoDao.updateLastLog(
      stationKey,
      { receivedAt, measuringLogs },
      stationAutoModel
    )
    return { success: true, data: resStationAuto }
  } catch (e) {
    return { error: true, message: e.message }
  }
}
