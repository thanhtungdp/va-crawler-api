import fs from 'fs'
import path from 'path'
import moment from 'moment'
import logger from 'logger'
import warningLevels from 'constants/warningLevels'
import importData from './importData'
import moveFile from './moveFile'

function repleaceMeasureLog ({ warningConfigs, measureLog }) {
  measureLog.warningLevel = null
  // check vuot nguong va chuan bi vuot nguong
  if (measureLog && measureLog.value && measureLog.maxLimit) {
    // Check vuot nguong
    if (measureLog.value >= measureLog.maxLimit) {
      measureLog.warningLevel = warningLevels.EXCEEDED
    } else if (
      measureLog.value / measureLog.maxLimit * 100 >=
      warningConfigs.exceededPreparing.value
    ) {
      // Check chuan bi vuot
      measureLog.warningLevel = warningLevels.EXCEEDED_PREPARING
    }
  }
  return measureLog
}

export function getDataImport ({
  station,
  receivedAt,
  measureData,
  warningConfigs
}) {
  let dataImport = {
    receivedAt: receivedAt,
    measuringLogs: {}
  }
  station.measuringList.forEach(item => {
    let measureFinded = measureData.find(
      measureSoure => measureSoure.measureName === item.measuringDes
    )
    if (measureFinded) {
      dataImport.measuringLogs[item.measuringDes] = repleaceMeasureLog({
        warningConfigs,
        measureLog: {
          value: measureFinded.value * item.ratio,
          minLimit: item.minLimit,
          maxLimit: item.maxLimit,
          statusDevice: measureFinded.statusDevice
        }
      })
    }
  })
  return dataImport
}

export async function changeFileName (filename) {
  let newFileName = path.join(
    path.dirname(filename),
    'IMPORTED_' + path.basename(filename)
  )
  await fs.renameSync(filename, newFileName)
}

export async function sv24Readfile ({ filename, showLog = true }) {
  try {
    let data = await fs.readFileSync(filename, 'utf8')
    let receivedAt = ''
    const measureData = data
      .split(`\n`)
      .map(row => {
        /*eslint-disable*/
        if (row.trim() !== '') {
          let rowValues = row
            .replace(`/[^\w\s]/gi`, ' ')
            .replace(/\t/g, ' ')
            .split(' ')
          const receivedTime = moment(rowValues[3], 'YYYYMMDDHHmmss')
          /* eslint-enable */
          if (!receivedTime.isValid()) {
            throw new Error('Time not valid')
          }
          if (receivedAt === '') {
            receivedAt = receivedTime.toDate()
          }
          return {
            measureName: rowValues[0],
            value: isNaN(Number.parseFloat(rowValues[1]))
              ? null
              : Number.parseFloat(rowValues[1]),
            unit: rowValues[2],
            time: receivedTime.toDate(),
            statusDevice: rowValues[4]
              ? Number.parseInt(rowValues[4].replace(/[^0-9.]/g, ''))
              : 0
          }
        }
        return null
      })
      .filter(measuring => !!measuring)
    if (showLog) {
      logger.info(`${filename} readed`)
    }
    if (!receivedAt) {
      throw new Error('Time not valid')
    }
    return {
      measureData,
      receivedAt
    }
  } catch (e) {
    moveFile({ filename, type: 'error' })
    logger.error(`${filename} not read`)
    return { error: true }
  }
}

export async function sv4ReadAndWriteToServer ({
  filename,
  station,
  organization,
  warningConfigs,
  q
}) {
  try {
    const { measureData, receivedAt } = await sv24Readfile({
      filename,
      showLog: false
    })
    let res = await importData({
      organization,
      stationKey: station.stationAuto,
      data: getDataImport({
        station,
        measureData,
        receivedAt,
        warningConfigs
      })
    })
    if (res.success) {
      changeFileName(filename)
      logger.info(`imported ${filename}`)
    } else {
      q.push({
        func: sv4ReadAndWriteToServer,
        params: { filename, organization, station, q }
      })
      logger.error(`danger ${filename} - ${res.message} `)
    }
    return res
  } catch (e) {
    logger.error(`ex ${filename} - ${e} `)
    return false
  }
}

export default sv24Readfile
