import config from 'config'
import createStationAutoModel from 'models/StationAuto'
import organizationDao from 'dao/organizationDao'
import path from 'path'
import stationAutoDao from 'dao/stationAutoDao'
import { createConnection } from 'shared/mongo-utils/connect'
import Queue from 'better-queue'
import CategoryApi from 'api/CategoryApi'
import logger from 'logger'
import watchFolder from './watchFolder'

export const q = new Queue(
  async function (input, cb) {
    if (!input.length) {
      const { func, params } = input
      await func(params)
      cb(null)
    } else {
      for (let i = 0; i < input.length; i++) {
        const { func, params } = input[i]
        await func(params)
      }
      cb(null)
    }
  },
  { batchSize: 1, batchDelayTimeout: 1000, afterProcessDelay: 300 }
)

async function getStations (organization) {
  try {
    const mongoDbConnect = await createConnection(organization.databaseInfo)
    const stationAutoModel = createStationAutoModel(mongoDbConnect)
    const stations = await stationAutoDao.getConfiguredLogger(stationAutoModel)
    return { success: true, stations: stations }
  } catch (e) {
    return { error: true, message: e.message }
  }
}

export async function getOrganizations () {
  const organizations = await organizationDao.findOnDemand()
  const promises = organizations.map(async organization => {
    const { success, stations } = await getStations(organization)
    return {
      organization: organization,
      stations: success ? stations : []
    }
  })
  const newOrganizations = []
  for (const promise of promises) {
    let organization = await promise
    newOrganizations.push(organization)
  }
  return newOrganizations
}

export function registerStation ({ organization, station, warningConfigs }) {
  const folder = path.join(config.folder.ftpPath, station.path)
  q.push({
    params: { organization, station, folder, warningConfigs },
    func: watchFolder
  })
}

export async function getSystemConfigs () {
  try {
    const configsSystem = await CategoryApi.getSysConfig()
    return configsSystem.data.stationAuto.warning
  } catch (e) {
    logger.error(`category API - ${e.message}`)
    return false
  }
}

export async function registerOrganizations (cbx) {
  const organizations = await getOrganizations()
  const warningConfigs = await getSystemConfigs()
  if (!warningConfigs) {
    logger.error('CATEGORY-API error')
    return { error: true, message: 'Get configs system error.' }
  }
  organizations.forEach(({ organization, stations }) => {
    stations.forEach(station => {
      cbx({ organization, station, warningConfigs })
    })
  })
}

export default async function registerOrganizationsWithStation () {
  registerOrganizations(registerStation)
}
