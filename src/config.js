if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

export default {
  folder: {
    ftpPath: process.env.FTP_PATH,
    ftpErrorPath: process.env.FTP_ERROR_PATH,
    ftpImportedPath: process.env.FTP_IMPORTED_PATH
  },
  MONGODB_OPTIONS: {
    database: process.env.MONGODB_URL,
    db_options: {
      native_parser: true,
      poolSize: 5,
      user: process.env.MONGODB_USER,
      pass: process.env.MONGODB_PASS,
      promiseLibrary: require('bluebird'),
      autoReconnect: true
    }
  },
  CATEGORY_API: process.env.CATEGORY_API,
  QUEUE_TOTAL_READ: process.env.QUEUE_TOTAL_READ,
  QUEUE_TOTAL_READ_CHILD: process.env.QUEUE_TOTAL_READ_CHILD
}
