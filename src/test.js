import path from 'path'
import { watchFolderForTest } from './lib/watchFolder'
import logger from 'logger'
import Queue from 'better-queue'
import config from 'config'
import manageConnectDatabase from 'utils/db'

console.log(config)

export const q = new Queue(
  async function (input, cb) {
    for (let i = 0; i < input.length; i++) {
      const { func, params } = input[i]
      await func(params)
    }
    cb(null)
  },
  { batchSize: 2, batchDelayTimeout: 1000, afterProcessDelay: 1500 }
)

const stations = ['TTH_TQT_KHI', 'TUNG']

async function main () {
  logger.info('Start watch')
  manageConnectDatabase()
  stations.forEach(station => {
    const folder = path.join(__dirname, `../datas/envisoft_cem/${station}`)
    q.push({ params: { folder }, func: watchFolderForTest })
  })
  q.on('empty', () => {
    console.log('done all')
  })
}

main()
