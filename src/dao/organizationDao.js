import Organization from 'models/Organization'

export default {
  async findOnDemand () {
    const results = await Organization.find(
      { 'databaseInfo.onDemand': true },
      {
        _id: 1,
        databaseInfo: 1
      }
    )
    return results
  }
}
