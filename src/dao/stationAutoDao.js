const stationAutoDao = {
  async updateLastLog (key, { receivedAt, measuringLogs }, Model) {
    let stationAuto = await Model.findOne({ key })
    if (!stationAuto) return false
    let lastLogDB = stationAuto.lastLog
    let receivedAtClient = new Date(receivedAt)
    if (lastLogDB && lastLogDB.receivedAt) {
      let receivedAtDB = new Date(lastLogDB.receivedAt)
      // Neu du lieu gui ve la thoi gian truoc do => khong update vao lastLog
      if (receivedAtDB > receivedAtClient) return null
    }
    let item = await Model.findOneAndUpdate(
      { key },
      {
        $set: {
          lastLog: { receivedAt, measuringLogs }
        }
      },
      { new: true }
    )
    return item
  },

  /**
   * Get station auto configured
   * Use for crawler API
   * @param query
   * @return [stationAuto, fileName, path, measuringList :[{measuringDes, measuringSrc, ratio, minLimit, maxLimit}]
   */
  async getConfiguredLogger (Model, query = {}) {
    let listItem = await Model.find(
      {
        $and: [{ configLogger: { $exists: true } }, query]
      },
      {
        key: 1,
        measuringList: 1,
        configLogger: 1
      }
    )
    let formatedList = []
    listItem.forEach(item => {
      let measuringList = item.measuringList
      let configLogger = item.configLogger
      if (
        configLogger &&
        measuringList &&
        configLogger.fileName &&
        configLogger.path &&
        configLogger.measuringList
      ) {
        const mList = []
        let measuringLogger = configLogger.measuringList
        measuringList.map(m => {
          measuringLogger.map(mLogger => {
            if (mLogger.measuringDes && mLogger.measuringDes === m.key) {
              mList.push({
                ...mLogger,
                minLimit: m.minLimit,
                maxLimit: m.maxLimit
              })
            }
          })
        })
        if (mList.length > 0) {
          formatedList.push({
            stationAuto: item.key,
            fileName: configLogger.fileName,
            path: configLogger.path,
            measuringList: mList
          })
        }
      }
    })
    return formatedList
  }
}

export default stationAutoDao
