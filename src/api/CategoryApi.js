import config from 'config'
import { getFetch } from 'utils/fetch'

export function getSysConfig () {
  return getFetch(`${config.CATEGORY_API}/config`)
}

export default {
  getSysConfig
}
