import manageConnectDatabase from 'utils/db'
import readAllFolder from './lib/readAllFolder'

async function main () {
  await manageConnectDatabase()
  try {
    readAllFolder()
  } catch (e) {
    console.log(e)
  }
}

main()
