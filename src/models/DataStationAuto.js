import mongoose from 'mongoose'

export const prefix = 'data-station-'

function getTableName (stationName) {
  return prefix + stationName
}

export default function createDataModel (stationName, conn) {
  const tableName = getTableName(stationName)
  if (conn.models && conn.models[tableName]) {
    return conn.models[tableName]
  } else {
    let schema = new mongoose.Schema({
      receivedAt: { type: Date, default: Date.now },
      measuringLogs: Object,
      createdAt: { type: Date, default: Date.now },
      updatedAt: { type: Date, default: Date.now }
    })
    return conn.model(tableName, schema)
  }
}
