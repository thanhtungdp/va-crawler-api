import mongoose from 'mongoose'

export default function createDataModel (conn) {
  if (conn.models && conn.models['station-autos']) {
    return conn.models['station-autos']
  } else {
    let schema = new mongoose.Schema({
      key: String,
      name: String,
      stationType: Object,
      address: String,
      mapLocation: Object,
      emails: Object,
      phones: Object,
      options: Object,
      measuringList: Object,
      lastLog: Object,
      image: Object,
      configLogger: {
        type: Object,
        default: { fileName: '', path: '', measuringList: [] }
      },
      removeStatus: {
        type: Object,
        default: { allowed: false, removeAt: null }
      },
      createdAt: { type: Date, default: Date.now },
      updatedAt: { type: Date, default: Date.now }
    })
    return conn.model('station-autos', schema)
  }
}
