module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    {
      name: 'iLotusLand_crawler_rewrite_api',
      script: 'bootstrap.js',
      env_production: {
        NODE_ENV: 'development',
        NODE_PATH: './src'
      }
    }
  ]
}
